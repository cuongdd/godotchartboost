extends Node

var chartboost = null

func _ready():
	if(Globals.has_singleton("Chartboost")):
		chartboost = Globals.get_singleton("Chartboost")
		chartboost.init()

func showInterstitial():
	if chartboost != null:
		chartboost.showInterstitial()

func showMoreApps():
	if chartboost != null:
		chartboost.showMoreApps()

func cacheInterstitial():
	if chartboost != null:
		chartboost.cacheInterstitial()

func cacheMoreApps():
	if chartboost != null:
		chartboost.cacheMoreApps()

func showRewardedVideo():
	if chartboost != null:
		chartboost.showRewardedVideo()

func cacheRewardedVideo():
	if chartboost != null:
		chartboost.cacheRewardedVideo() 

func _on_show_released():
	showInterstitial()


func _on_show1_released():
	showRewardedVideo()

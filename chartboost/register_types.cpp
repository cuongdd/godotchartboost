#include "register_types.h"
#include "object_type_db.h"
#include "core/globals.h"
#include "ios/src/godotchartboost.h"

void register_chartboost_types() {
    Globals::get_singleton()->add_singleton(Globals::Singleton("Chartboost", memnew(GodotChartboost)));
}

void unregister_chartboost_types() {
}

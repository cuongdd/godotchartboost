def can_build(plat):
	return plat == "iphone" or plat == "android"

def configure(env):
	if env['platform'] == 'android':
		env.android_add_dependency("compile 'com.google.android.gms:play-services-ads:8.+'")
		env.android_add_dependency("compile files('../../../modules/chartboost/android/lib/chartboost.jar')")
		env.android_add_to_manifest("android/AndroidManifestChunk.xml")
		env.android_add_java_dir("android")
		#env.disable_module()
	elif env['platform'] == "iphone":
		env.Append(FRAMEWORKPATH=['modules/chartboost/ios/lib'])
		env.Append(LINKFLAGS=['-ObjC', '-framework', 'Chartboost', '-framework', 'AdSupport'])

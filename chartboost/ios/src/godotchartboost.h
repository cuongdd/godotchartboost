#ifndef __GODOTCHARTBOOST_H__
#define __GODOTCHARTBOOST_H__

#include "reference.h"

class GodotChartboost : public Reference {
    OBJ_TYPE(GodotChartboost,Reference);

    static void _bind_methods();
    //static GodotChartboost* instance;

    bool initialized;
    int call_id;

public:
    void init(int call_id);

    void cacheInterstitial(const String& location);
    bool hasInterstitial(const String& location);
    void showInterstitial(const String& location);
    void cacheMoreApps(const String& location);
    bool hasMoreApps(const String& location);
    void showMoreApps(const String& location);
    void cacheRewardVideo(const String& location);
    bool hasRewardVideo(const String& location);
    void showRewardVideo(const String& location);
    void setAutoCacheAds(bool autoCacheAds);
    bool getAutoCacheAds();
    void setShouldRequestInterstitialsInFirstSession(bool shouldRequest);
    bool isAnyViewVisible();
    void closeImpression();

    GodotChartboost();
    ~GodotChartboost();
    
    int get_call_id();
    
};

#endif
